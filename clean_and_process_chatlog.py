import pandas as pd
import os
from dotenv import load_dotenv
from db_connector import insert_to_db, query_from_db
import logging
from datetime import datetime, date, timedelta, timezone
from utils.helper import deEmojify, remove_col_str, remove_col_white_space

logging.basicConfig(format='%(asctime)s %(message)s', filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

load_dotenv()

SCHEMA = os.environ.get("SCHEMA")
QUERY_TABLE_NAME = os.environ.get('QUERY_TABLE_NAME')
INSERT_TABLE_NAME = os.environ.get("INSERT_TABLE_NAME")
TIMEZONE = os.environ.get('TIMEZONE')

if SCHEMA:
    QUERY_TABLE_NAME = SCHEMA + "." + QUERY_TABLE_NAME
    INSERT_TABLE_NAME = SCHEMA + "." + INSERT_TABLE_NAME

try:
    BLACK_LIST_SENDER_ID = [os.environ.get("BLACK_LIST_SENDER_ID").split(",")]
except:
    BLACK_LIST_SENDER_ID = []


def get_data_from_db(start_date, end_date):
    """
    Get data from database
    :param start_date:
    :param end_date:
    :return: pd.DataFrame
    """
    start_time_1 = '08:00:00'
    end_time_1 = '12:05:00'

    start_time_2 = '14:00:00'
    end_time_2 = '17:05:00'

    query = """
    SELECT * 
    FROM {table_name}
    WHERE (created_time >= '{start_date}' AND created_time <= '{end_date}')
    AND ((cast(created_time as time) >= '{morning_start_time}' AND cast(created_time as time) <= '{morning_end_time}') OR (cast(created_time as time) >= '{afternoon_start_time}' AND cast(created_time as time) <= '{afternoon_end_time}'))
    AND (week_day <> 6 AND week_day <> 7)
    """
    query = query.format(table_name=QUERY_TABLE_NAME,
                         start_date=start_date,
                         end_date=end_date,
                         morning_start_time=start_time_1,
                         morning_end_time=end_time_1,
                         afternoon_start_time=start_time_2,
                         afternoon_end_time=end_time_2)

    df = query_from_db(query=query)
    return df


def clean_rasa_chatlog(df):
    """
    Remove col str, remove emoji, strip white space
    :rtype: pd.DataFrame
    """
    df["user_message_correction"] = df["user_message"]
    df = remove_col_str(df=df, col_name="user_message_correction")
    df = deEmojify(df=df, col_name="user_message_correction", og_col_name="user_message_correction")
    # rasa_chatlog = correction_message(df=rasa_chatlog, col_name="user_message_correction",
    #                                   og_col_name="user_message_correction")
    df = remove_col_white_space(df=df, col_name="user_message_correction")
    return df


def clean_and_process_raw_chatlog(df: pd.DataFrame):
    """
    Clean and split chatlog to conversation
    :rtype: object
    """
    df = df[~df["sender_id"].isin(BLACK_LIST_SENDER_ID)]
    df = clean_rasa_chatlog(df)


def start_clean_and_process_chatlog():
    df = get_data_from_db("2020-11-20", "2020-11-27")
    df = clean_and_process_raw_chatlog(df)


if __name__ == '__main__':
    start_clean_and_process_chatlog()
