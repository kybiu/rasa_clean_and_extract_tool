import psycopg2
import os
from dotenv import load_dotenv
import pandas as pd
import psycopg2.extras as extras

load_dotenv()

HOST = os.environ.get("HOST")
PORT = os.environ.get("PORT")
DATABASE = os.environ.get("DATABASE")
USER = os.environ.get("USER")
PASSWORD = os.environ.get("PASSWORD")
SCHEMA = os.environ.get("SCHEMA")
QUERY_TABLE_NAME = os.environ.get('QUERY_TABLE_NAME')
INSERT_TABLE_NAME = os.environ.get("INSERT_TABLE_NAME")

param_dict = {
    "host": HOST,
    "port": PORT,
    "database": DATABASE,
    "user": USER,
    "password": PASSWORD
}


def create_table(conn, table_name):
    cursor = conn.cursor()
    query = """
    CREATE TABLE IF NOT EXISTS {table} (
        id SERIAL PRIMARY KEY,
        message_id TEXT,
        sender_id BIGINT,
        sender TEXT,
        user_message TEXT,
        bot_message TEXT,
        utter_name TEXT,
        intent TEXT,
        entities TEXT,
        created_time TIMESTAMP,
        week_day INT,
        attachments TEXT)
     """
    query = query.format(table=table_name)
    cursor.execute(query)
    conn.commit()

    a = 0


def connect_db():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    print("Connection successful")
    return conn


def insert_to_db(df: pd.DataFrame, table_name):
    """
       Using psycopg2.extras.execute_values() to insert the dataframe
       """
    conn = connect_db()
    # Create a list of tupples from the dataframe values
    tuples = [tuple(x) for x in df.to_numpy()]
    # Comma-separated dataframe columns
    cols = ','.join(list(df.columns))

    # Create table if not exist
    create_table(conn, table_name)

    # SQL quert to execute
    query = "INSERT INTO %s(%s) VALUES %%s" % (table_name, cols)
    cursor = conn.cursor()
    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("execute_values() done")
    cursor.close()


def query_from_db(query):
    conn = connect_db()
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        data = cursor.fetchall()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("execute_values() done")
    cursor.close()
    col_names = [x.name for x in cursor.description]

    df = pd.DataFrame(data=data, columns=col_names)
    return df
